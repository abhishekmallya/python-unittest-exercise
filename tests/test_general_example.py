import unittest
from unittest.mock import patch
from src.general_example import GeneralExample

general_example_instance = GeneralExample()

class TestGeneral(unittest.TestCase):
    
    def flatten_dictionary(self):
        dictionary = {'key1': [3, 2, 1], 'key2': [42, 55, 10], 'key3': [0, 22]}
        flatten = [0, 1, 2, 3, 10, 22, 42, 55]
        self.assertEqual(general_example_instance.flatten_dictionary(dictionary), flatten)

    @patch("src.general_example.GeneralExample.load_employee_rec_from_database")
    def test_fetch_emp_details(self, mock_load_employee_rec):
        expected_emp_details = {
            'empId': 'emp002',
            'empName': 'Sam',
            'empSalary': '100000'
        }
        mock_load_employee_rec.return_value = ['emp002', 'Sam', '100000']

        emp_details = general_example_instance.fetch_emp_details()
        self.assertEqual(emp_details, expected_emp_details)

if __name__ == '__main__':
     unittest.main()